Design Decisions
- Dagger Hilt as DI framework. Other options are Koin or just a regular map (Service Locator pattern), but since I don't know your stack, I went with the most common.
- Internal classes on network module, only interfaces are public
- Repositories are built using the Dependency Inversion principle (The "D" in SOLID)
- [Object Mother](https://martinfowler.com/bliki/ObjectMother.html) patter on tests
- Preferred simplicity over convention, meaning that won't use fragments if it's not needed, wont create a UseCase class if there is no business logic, etc
- No domain module because no logic present (again, simplicity)
- Error handling across layers is simplistic. Right now it's treating all errors the same, but in a real scenario they should have different flows for different types (no connection, server error, invalid request, etc)
- Tests are in "user language" meaning that instead of saying "ViewModelTest Transitions view to Loading State" they say "ViewModelShould show loading before loading data"
- In tests, @test functions are first, and properties, setup, etc are at the end, to increase readability
- In unit tests, objects under verification are injected via constructor (DI pattern, not framework)
- I've considered Chart style and formats not to be customizable due to time constraint. Else would have been injected into the view
- Not diving into Dark theme

CI
- Publish Internal is pointing to gitlab artifacts, in real scenarios would be either Firebase App Distribution, or another similar solution
- UI-Tests step is not implemented due to time constraints, but could point to Firebase Test Lab or any other device farm service
- In real scenario, app version should be generated on the CI and injected into the app
- Deploy step is not real for obvious reasons
- Pipeline time could be optimized sending build cache across steps

Smells
- Dagger forces api dependencies on gradle (including them again on the main module just hides the problem)
- Dagger forces classes properties to be public on the Activity
- Can't test chart contents since its a plain view
