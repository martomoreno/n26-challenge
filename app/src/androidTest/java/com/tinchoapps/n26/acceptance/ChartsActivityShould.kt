package com.tinchoapps.n26.acceptance

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.ActivityScenarioRule
import com.tinchoapps.n26.ChartsActivity
import com.tinchoapps.n26.NetworkConfigurationModule
import com.tinchoapps.n26.R
import com.tinchoapps.n26.idlingresource.EspressoIdlingResource
import com.tinchoapps.n26.mother.a_mock_server
import com.tinchoapps.n26.mother.error_response
import com.tinchoapps.n26.mother.ok_response
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.runBlocking
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import javax.inject.Inject
import javax.inject.Named


private lateinit var server: MockWebServer

@UninstallModules(NetworkConfigurationModule::class)
@HiltAndroidTest
class ChartsActivityShould {

    @Test
    fun show_market_price_chart() {
        given_a_server_answering_ok()

        onView(withId(R.id.marketPrice))
            .check(matches(isDisplayed()))
    }

    @Test
    fun show_market_price_title_correctly() {
        given_a_server_answering_ok()

        onView(allOf(withId(R.id.titleView), isDescendantOfA(withId(R.id.marketPrice))))
            .check(matches(withText(R.string.chart_title_market_price)))
            .check(matches(isDisplayed()))
    }

    @Test
    fun show_market_cap_chart() {
        given_a_server_answering_ok()

        onView(withId(R.id.marketCap))
            .check(matches(isDisplayed()))
    }

    @Test
    fun show_market_cap_title_correctly() {
        given_a_server_answering_ok()

        onView(allOf(withId(R.id.titleView), isDescendantOfA(withId(R.id.marketCap))))
            .check(matches(withText(R.string.chart_title_market_cap)))
            .check(matches(isDisplayed()))
    }

    @Test
    fun show_trade_volume_chart() {
        given_a_server_answering_ok()

        onView(withId(R.id.tradeVolume))
            .check(matches(isDisplayed()))
    }

    @Test
    fun show_trade_volume_title_correctly() {
        given_a_server_answering_ok()

        onView(allOf(withId(R.id.titleView), isDescendantOfA(withId(R.id.tradeVolume))))
            .check(matches(withText(R.string.chart_title_exchange_volume)))
            .check(matches(isDisplayed()))
    }

    @Test
    fun show_retry_view_on_error() {
        given_a_server_answering_error()

        onView(withId(R.id.retryView))
            .check(matches(isCompletelyDisplayed()))
        onView(withId(R.id.contentView))
            .check(matches(not(isDisplayed())))
    }

    private fun given_a_server_answering_error() {
        server.enqueue(error_response())
    }

    private fun given_a_server_answering_ok() {
        server.enqueue(ok_response())
        server.enqueue(ok_response())
        server.enqueue(ok_response())
    }

    @Before
    fun setUp() {
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
        hiltRule.inject()
    }

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    }

    @Inject
    lateinit var okHttp: OkHttpClient

    @get:Rule(order = 0)
    val hiltRule = HiltAndroidRule(this)

    @get:Rule(order = 1)
    val activityRule: ActivityScenarioRule<ChartsActivity> =
        ActivityScenarioRule(ChartsActivity::class.java)

    @Module
    @InstallIn(SingletonComponent::class)
    class TestNetworkConfiguration {
        @Provides
        @Named("base_url")
        fun providesBlockchainBaseUrl(): String {
            server = a_mock_server()
            return "http://localhost:${server.port}"
        }
    }
}