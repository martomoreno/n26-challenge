package com.tinchoapps.n26.mother

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer


fun a_mock_server(): MockWebServer {
    val server = MockWebServer()
    server.start()
    return server
}

fun ok_response(): MockResponse {
    return MockResponse().setBody(
        """
            {
              "status": "ok",
              "name": "Market Price (USD)",
              "unit": "USD",
              "period": "day",
              "description": "Average USD market price across major bitcoin exchanges.",
              "values": [
                        {"x":1616025600,"y":58913.0},
                        {"x":1616112000,"y":57665.9},
                        {"x":1616198400,"y":58075.1},
                        {"x":1616284800,"y":58085.8},
                        {"x":1616371200,"y":57411.17},
                        {"x":1616457600,"y":54204.96}
                    ]
            }
            """.trimIndent()
    )
}

fun error_response(): MockResponse =
    MockResponse().setResponseCode(400)
