package com.tinchoapps.n26

import android.os.Bundle
import android.view.View.*
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.github.mikephil.charting.data.Entry
import com.tinchoapps.n26.viewmodel.ChartView
import com.tinchoapps.n26.viewmodel.ChartViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_charts.*
import javax.inject.Inject

@AndroidEntryPoint
class ChartsActivity : AppCompatActivity(), ChartView {

    @Inject
    lateinit var viewModelFactory: ChartViewModel.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_charts)
        title = getString(R.string.title_chart_activity)
        marketPrice.title = getString(R.string.chart_title_market_price)
        marketCap.title = getString(R.string.chart_title_market_cap)
        tradeVolume.title = getString(R.string.chart_title_exchange_volume)

        val viewModel: ChartViewModel by viewModels(factoryProducer = { viewModelFactory })
        viewModel.attachView(this)
    }

    override fun fillMarketPrice(entries: List<Entry>) {
        marketPrice.addDataPoint(entries)
        contentView.visibility = VISIBLE
        loadingView.visibility = GONE
        retryView.visibility = GONE
    }

    override fun fillMarketCap(entries: List<Entry>) {
        marketCap.addDataPoint(entries)
    }

    override fun fillTradeVolume(entries: List<Entry>) {
        tradeVolume.addDataPoint(entries)
    }

    override fun showRetry(onRetry: () -> Unit) {
        contentView.visibility = GONE
        loadingView.visibility = GONE
        retryView.visibility = VISIBLE
        retryView.setOnClickListener { onRetry() }
    }

    override fun showLoading() {
        contentView.visibility = GONE
        loadingView.visibility = VISIBLE
        retryView.visibility = GONE
    }
}