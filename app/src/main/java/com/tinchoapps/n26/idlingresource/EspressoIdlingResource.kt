package com.tinchoapps.n26.idlingresource

import androidx.test.espresso.idling.CountingIdlingResource

object EspressoIdlingResource {

    private const val name = "EspressoIdlingResource"

    @JvmField
    val countingIdlingResource = CountingIdlingResource(name)

    fun increment() {
        countingIdlingResource.increment()
    }

    fun decrement() {
        if (!countingIdlingResource.isIdleNow) {
            countingIdlingResource.decrement()
        }
    }
}

inline fun <T> makeIdling(function: () -> T): T {
    EspressoIdlingResource.increment()
    return try {
        function()
    } finally {
        EspressoIdlingResource.decrement()
    }
}
