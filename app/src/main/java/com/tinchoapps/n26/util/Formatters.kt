package com.tinchoapps.n26.util

import java.text.NumberFormat
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

fun Float.toDollarString(): String {
    val formatter = NumberFormat.getCurrencyInstance(Locale.getDefault())
    formatter.currency = Currency.getInstance("USD")
    formatter.maximumFractionDigits = 0
    return formatter.format(this)
}

fun Float.toChartDate(): String {
    return DateTimeFormatter.ofPattern("dd-MMM")
        .withZone(ZoneId.systemDefault())
        .format(Instant.ofEpochSecond(this.toLong()))
}