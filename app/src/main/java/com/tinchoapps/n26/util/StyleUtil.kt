package com.tinchoapps.n26.util

import android.content.Context

import android.util.TypedValue
import androidx.annotation.AttrRes


fun findAttributeInTheme(context: Context,@AttrRes resId: Int): Int {
    val value = TypedValue()
    context.theme.resolveAttribute(resId, value, true)
    return value.data
}