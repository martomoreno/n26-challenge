package com.tinchoapps.n26.view

import android.content.Context
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import com.tinchoapps.n26.R
import com.tinchoapps.n26.util.toChartDate
import com.tinchoapps.n26.util.toDollarString
import kotlinx.android.synthetic.main.view_marker.view.*

class ChartMarker(context: Context) : MarkerView(context, R.layout.view_marker) {
    override fun refreshContent(entry: Entry, highlight: Highlight) {
        markerDateView.text = entry.x.toChartDate()
        markerPriceView.text = entry.y.toDollarString()
        super.refreshContent(entry, highlight)
    }

}