package com.tinchoapps.n26.view

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.setPadding
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.tinchoapps.n26.R
import com.tinchoapps.n26.util.findAttributeInTheme
import com.tinchoapps.n26.view.format.MoneyFormatter
import com.tinchoapps.n26.view.format.MonthFormatter
import kotlinx.android.synthetic.main.view_loading_chart.view.*

class ChartView : ConstraintLayout {
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, attributeSetId: Int) : super(
        context,
        attrs,
        attributeSetId
    )

    private val dataSetColor = findAttributeInTheme(context, R.attr.colorOnSecondary)
    private val dataSetTextColor = findAttributeInTheme(context, R.attr.colorOnSecondary)
    private val nodeColor = findAttributeInTheme(context, R.attr.colorPrimary)
    private val textSize = resources.getDimension(R.dimen.chart_text_size)

    var title: String
        get() = titleView.text.toString()
        set(value) {
            titleView.text = value
        }

    init {
        inflate(context, R.layout.view_loading_chart, this)

        styleChart(lineChart)
        setPadding(resources.getDimension(R.dimen.margin_small).toInt())
        setBackgroundResource(R.drawable.card_background)
    }

    fun addDataPoint(entries: List<Entry>) {
        val dataSet = LineDataSet(entries, "")
        styleDataSet(dataSet)
        lineChart.data = LineData(dataSet)
        lineChart.notifyDataSetChanged()
        lineChart.invalidate()
    }

    private fun styleChart(lineChart: LineChart) {
        with(lineChart) {
            setDrawGridBackground(false)
            description.isEnabled = false
            legend.isEnabled = false
            axisRight.isEnabled = false
            axisLeft.isEnabled = false
            setScaleEnabled(false)
            isDoubleTapToZoomEnabled = false
            setPinchZoom(false)
            xAxis.isEnabled = true
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            xAxis.valueFormatter = MonthFormatter()
            xAxis.setDrawGridLines(false)
            xAxis.setDrawAxisLine(false)
            xAxis.textColor = dataSetTextColor
            bindCustomMarker(lineChart)
        }
    }

    private fun bindCustomMarker(lineChart: LineChart) {
        val customMarker = ChartMarker(context)
        lineChart.marker = customMarker
        customMarker.chartView = lineChart
    }

    private fun styleDataSet(dataSet: LineDataSet) {
        with(dataSet) {
            disableDashedLine()
            color = dataSetColor
            valueTextSize = textSize
            lineWidth = 1.5f
            circleColors = listOf(nodeColor)
            valueFormatter = MoneyFormatter()
            setValueTextColors(listOf(dataSetTextColor))
            setDrawHighlightIndicators(false)
            mode = LineDataSet.Mode.CUBIC_BEZIER
        }
    }
}

