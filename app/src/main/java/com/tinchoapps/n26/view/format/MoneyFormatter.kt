package com.tinchoapps.n26.view.format

import com.github.mikephil.charting.formatter.ValueFormatter
import com.tinchoapps.n26.util.toDollarString
import java.text.NumberFormat
import java.util.*

class MoneyFormatter : ValueFormatter() {
    override fun getFormattedValue(value: Float): String {
        return value.toDollarString()
    }
}