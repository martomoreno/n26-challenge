package com.tinchoapps.n26.view.format

import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.formatter.ValueFormatter
import com.tinchoapps.n26.util.toChartDate
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter

class MonthFormatter : ValueFormatter() {
    override fun getAxisLabel(value: Float, axis: AxisBase?): String = value.toChartDate()
}