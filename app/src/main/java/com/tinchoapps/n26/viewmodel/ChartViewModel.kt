package com.tinchoapps.n26.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.github.mikephil.charting.data.Entry
import com.tinchoapps.n26.idlingresource.makeIdling
import com.tinchoapps.n26.infrastructure.blockchain.BlockchainData
import com.tinchoapps.n26.infrastructure.blockchain.Chart.*
import com.tinchoapps.n26.infrastructure.blockchain.MoneyToTime
import com.tinchoapps.n26.infrastructure.blockchain.TimeSpan.THREE_DAYS
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class ChartViewModel
@Inject constructor(private val repository: BlockchainData) : ViewModel() {
    fun attachView(view: ChartView) {
        loadData(view)
    }

    private fun loadData(view: ChartView) {
        view.showLoading()
        val handler = CoroutineExceptionHandler { _: CoroutineContext, _: Throwable ->
            view.showRetry { loadData(view) }
        }
        viewModelScope.launch(handler) {
            makeIdling {
                val priceEntries = repository.findChartEntries(MARKET_PRICE, THREE_DAYS)
                    .map { it.toEntry() }
                val capEntries = repository.findChartEntries(MARKET_CAP, THREE_DAYS)
                    .map { it.toEntry() }
                val tradeEntries = repository.findChartEntries(TRADE_VOLUME, THREE_DAYS)
                    .map { it.toEntry() }

                view.fillMarketPrice(priceEntries)
                view.fillMarketCap(capEntries)
                view.fillTradeVolume(tradeEntries)
            }
        }
    }

    class Factory @Inject
    constructor(private val repository: BlockchainData) : ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            when (modelClass.isAssignableFrom(ChartViewModel::class.java)) {
                true -> return ChartViewModel(repository) as T
                else -> throw IllegalArgumentException("Unknown ViewModel class ${modelClass.canonicalName}")
            }
        }
    }
}

interface ChartView {
    fun fillMarketPrice(entries: List<Entry>)
    fun fillMarketCap(entries: List<Entry>)
    fun fillTradeVolume(entries: List<Entry>)
    fun showRetry(onRetry: () -> Unit)
    fun showLoading()
}

private fun MoneyToTime.toEntry(): Entry =
    Entry(timestamp.toEpochMilli().toFloat(), price.toFloat())
