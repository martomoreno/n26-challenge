package com.tinchoapps.n26.mother

import com.tinchoapps.n26.infrastructure.GenericInfrastructureException
import com.tinchoapps.n26.infrastructure.blockchain.BlockchainData
import com.tinchoapps.n26.infrastructure.blockchain.Chart
import com.tinchoapps.n26.infrastructure.blockchain.MoneyToTime
import io.mockk.coEvery
import io.mockk.mockk
import java.lang.Exception


fun a_mock_repository(
    withMarketPrice: List<MoneyToTime> = emptyList(),
    withMarketCap: List<MoneyToTime> = emptyList(),
    withTradeVolume: List<MoneyToTime> = emptyList()
): BlockchainData {
    val repository = mockk<BlockchainData>()
    coEvery { repository.findChartEntries(Chart.MARKET_PRICE, any()) } returns withMarketPrice
    coEvery { repository.findChartEntries(Chart.MARKET_CAP, any()) } returns withMarketCap
    coEvery { repository.findChartEntries(Chart.TRADE_VOLUME, any()) } returns withTradeVolume
    return repository
}

fun a_failing_repository(): BlockchainData {
    val repository = mockk<BlockchainData>()
    coEvery { repository.findChartEntries(any(), any()) } throws
            GenericInfrastructureException(Exception())
    return repository
}