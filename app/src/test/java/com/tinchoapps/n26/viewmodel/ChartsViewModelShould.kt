package com.tinchoapps.n26.viewmodel

import com.github.mikephil.charting.data.Entry
import com.tinchoapps.n26.infrastructure.blockchain.Chart.*
import com.tinchoapps.n26.infrastructure.blockchain.MoneyToTime
import com.tinchoapps.n26.mother.a_failing_repository
import com.tinchoapps.n26.mother.a_mock_repository
import io.mockk.*
import kotlinx.coroutines.*
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test

class ChartsViewModelShould {
    @Test
    fun fetch_all_from_repository() {
        val repository = a_mock_repository()
        val viewModel = ChartViewModel(repository)

        viewModel.attachView(chartView)

        coVerify(exactly = 1) { repository.findChartEntries(MARKET_PRICE, any()) }
        coVerify(exactly = 1) { repository.findChartEntries(MARKET_CAP, any()) }
        coVerify(exactly = 1) { repository.findChartEntries(TRADE_VOLUME, any()) }
    }

    @Test
    fun fill_market_price_in_view() {
        val repository = a_mock_repository(
            withMarketPrice = listOf(
                MoneyToTime.from(epoch0, price0),
                MoneyToTime.from(epoch1, price1)
            )
        )
        val viewModel = ChartViewModel(repository)

        viewModel.attachView(chartView)

        verify(exactly = 1) { chartView.fillMarketPrice(any()) }

        assertThat(marketPrice.isCaptured).isTrue
        assertThat(marketPrice.captured)
            .usingElementComparator(EntriesComparator())
            .containsExactly(
                Entry(epoch0.toFloat(), price0.toFloat()),
                Entry(epoch1.toFloat(), price1.toFloat())
            )
    }

    @Test
    fun fill_market_cap_in_view() {
        val repository = a_mock_repository(
            withMarketCap = listOf(
                MoneyToTime.from(epoch2, price2),
                MoneyToTime.from(epoch3, price3)
            )
        )
        val viewModel = ChartViewModel(repository)

        viewModel.attachView(chartView)

        verify(exactly = 1) { chartView.fillMarketCap(any()) }

        assertThat(marketCap.isCaptured).isTrue
        assertThat(marketCap.captured)
            .usingElementComparator(EntriesComparator())
            .containsExactly(
                Entry(epoch2.toFloat(), price2.toFloat()),
                Entry(epoch3.toFloat(), price3.toFloat()),
            )
    }

    @Test
    fun fill_trade_volume_in_view() {
        val repository = a_mock_repository(
            withTradeVolume = listOf(
                MoneyToTime.from(epoch4, price4),
                MoneyToTime.from(epoch5, price5)
            )
        )
        val viewModel = ChartViewModel(repository)

        viewModel.attachView(chartView)

        verify(exactly = 1) { chartView.fillTradeVolume(any()) }

        assertThat(tradeVolume.isCaptured).isTrue
        assertThat(tradeVolume.captured)
            .usingElementComparator(EntriesComparator())
            .containsExactly(
                Entry(epoch4.toFloat(), price4.toFloat()),
                Entry(epoch5.toFloat(), price5.toFloat()),
            )
    }

    @Test
    fun show_retry_screen_on_error() {
        val repository = a_failing_repository()
        val viewModel = ChartViewModel(repository)

        viewModel.attachView(chartView)

        coVerifyOrder {
            repository.findChartEntries(MARKET_PRICE, any())
            chartView.showRetry(any())
        }
    }

    @Test
    fun show_loading_before_loading_data() {
        val repository = a_mock_repository()
        val viewModel = ChartViewModel(repository)

        viewModel.attachView(chartView)

        verifyOrder {
            chartView.showLoading()
            chartView.fillMarketPrice(any())
        }
    }

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)
        setupChartViewSpy()
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    private fun setupChartViewSpy() {
        chartView = spyk()
        marketPrice = slot()
        marketCap = slot()
        tradeVolume = slot()
        every {
            chartView.fillMarketPrice(capture(marketPrice))
        } returns Unit
        every {
            chartView.fillMarketCap(capture(marketCap))
        } returns Unit
        every {
            chartView.fillTradeVolume(capture(tradeVolume))
        } returns Unit
        every {
            chartView.showRetry(any())
        } returns Unit
    }

    private val epoch0 = 1613174400L
    private val price0 = 43.03
    private val epoch1 = 1613260800L
    private val price1 = 44.04
    private val epoch2 = 1616274969L
    private val price2 = 1.094651337709375E12
    private val epoch3 = 1616275080L
    private val price3 = 1.0946517043625E12
    private val epoch4 = 1616198400L
    private val price4 = 5.12236900775E8
    private val epoch5 = 1616284800L
    private val price5 = 3.88273949242E8
    private lateinit var chartView: ChartView
    private lateinit var marketPrice: CapturingSlot<List<Entry>>
    private lateinit var marketCap: CapturingSlot<List<Entry>>
    private lateinit var tradeVolume: CapturingSlot<List<Entry>>

    @ExperimentalCoroutinesApi
    private val testDispatcher = TestCoroutineDispatcher()
}

private class EntriesComparator : Comparator<Entry> {
    override fun compare(entry: Entry, entry2: Entry): Int =
        compareValuesBy(entry, entry2, { it.x }, { it.y })
}
