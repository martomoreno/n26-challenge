package com.tinchoapps.n26

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Named

@Module
@InstallIn(SingletonComponent::class)
object NetworkConfigurationModule {
    @Provides
    @Named("base_url")
    fun providesBlockchainBaseUrl(): String =
        "https://api.blockchain.info"
}