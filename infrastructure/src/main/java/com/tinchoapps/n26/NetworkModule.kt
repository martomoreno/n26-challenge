package com.tinchoapps.n26

import com.tinchoapps.n26.infrastructure.blockchain.BlockchainData
import com.tinchoapps.n26.infrastructure.blockchain.RemoteBlockchainData
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Provides
    fun providesBlockchainData(retrofit: Retrofit): BlockchainData =
        RemoteBlockchainData(retrofit)

    @Provides
    fun providesRetrofit(@Named("base_url") baseUrl: String, client: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    fun providesOkHttpClient(): OkHttpClient = OkHttpClient.Builder().build()
}