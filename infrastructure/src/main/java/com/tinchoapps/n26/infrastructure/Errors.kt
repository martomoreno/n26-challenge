package com.tinchoapps.n26.infrastructure

import java.lang.Exception

class RequestFailed(message:String?) : Exception(message)

class CantFindRequestBody() : Exception()

class GenericInfrastructureException(exception: Exception) : Exception(exception)