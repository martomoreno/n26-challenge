package com.tinchoapps.n26.infrastructure.blockchain

import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


internal interface BlockchainApi {
    @GET("charts/{chart_name}/?format=json&sampled=true")
    suspend fun fetchChartEntries(
        @Path("chart_name") name: String,
        @Query("timespan") timespan: String
    ): Response<PriceEntriesDTO>
}

data class PriceEntriesDTO(val values: List<PriceDTO>)
data class PriceDTO(val x: Long, val y: Double)