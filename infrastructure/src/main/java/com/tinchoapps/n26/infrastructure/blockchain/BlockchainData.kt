package com.tinchoapps.n26.infrastructure.blockchain

import java.math.BigDecimal
import java.time.Instant

interface BlockchainData {
    suspend fun findChartEntries(chart: Chart, timeSpan: TimeSpan): List<MoneyToTime>
}

enum class TimeSpan {
    THREE_DAYS
}

enum class Chart {
    MARKET_PRICE,
    MARKET_CAP,
    TRADE_VOLUME
}

data class MoneyToTime(val timestamp: Instant, val price: BigDecimal) {
    companion object {
        fun from(epoch: Long, price: Double): MoneyToTime {
            return MoneyToTime(
                Instant.ofEpochMilli(epoch),
                BigDecimal(price)
            )
        }
    }
}