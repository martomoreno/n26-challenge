package com.tinchoapps.n26.infrastructure.blockchain

import com.tinchoapps.n26.infrastructure.CantFindRequestBody
import com.tinchoapps.n26.infrastructure.GenericInfrastructureException
import com.tinchoapps.n26.infrastructure.RequestFailed
import com.tinchoapps.n26.infrastructure.blockchain.Chart.*
import com.tinchoapps.n26.infrastructure.blockchain.TimeSpan.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.Exception

internal class RemoteBlockchainData(retrofit: Retrofit) : BlockchainData {
    private val api = retrofit.create(BlockchainApi::class.java)

    override suspend fun findChartEntries(chart: Chart, timeSpan: TimeSpan): List<MoneyToTime> {
        val response: Response<PriceEntriesDTO>
        try {
            response = api.fetchChartEntries(chart.toValue(), timeSpan.toValue())
        } catch (e: Exception) {
            throw GenericInfrastructureException(e)
        }
        val body: PriceEntriesDTO? = response.body()
        return when {
            response.isSuccessful.not() -> throw RequestFailed(response.errorBody()?.toString())
            body?.values == null -> throw CantFindRequestBody()
            else -> body.values.map {
                MoneyToTime.from(it.x, it.y)
            }
        }
    }
}

private fun TimeSpan.toValue(): String =
    when (this) {
        THREE_DAYS -> "3days"
    }

private fun Chart.toValue(): String =
    when (this) {
        MARKET_PRICE -> "market-price"
        MARKET_CAP -> "market-cap"
        TRADE_VOLUME -> "trade-volume"
    }
