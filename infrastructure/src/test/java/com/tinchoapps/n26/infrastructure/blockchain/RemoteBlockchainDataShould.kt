package com.tinchoapps.n26.infrastructure.blockchain

import com.tinchoapps.n26.infrastructure.CantFindRequestBody
import com.tinchoapps.n26.infrastructure.GenericInfrastructureException
import com.tinchoapps.n26.infrastructure.RequestFailed
import com.tinchoapps.n26.infrastructure.blockchain.Chart.*
import com.tinchoapps.n26.infrastructure.blockchain.TimeSpan.THREE_DAYS
import com.tinchoapps.n26.infrastructure.mother.*
import com.tinchoapps.n26.infrastructure.mother.a_mock_server
import com.tinchoapps.n26.infrastructure.mother.a_mock_server_failing_with_400
import com.tinchoapps.n26.infrastructure.mother.a_mock_server_with_empty_body
import com.tinchoapps.n26.infrastructure.mother.a_repository_pointing_to
import com.tinchoapps.n26.infrastructure.mother.queryParams
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class RemoteBlockchainDataShould {

    @Test
    fun fetch_market_price_from_the_correct_path() {
        runBlocking {
            val server = a_mock_server()
            val repository = a_repository_pointing_to(server.port)

            repository.findChartEntries(MARKET_PRICE, THREE_DAYS)

            assertThat(server.takeRequest().requestUrl!!.encodedPath)
                .isEqualTo("/charts/market-price/")
        }
    }

    @Test
    fun fetch_market_cap_from_the_correct_path() {
        runBlocking {
            val server = a_mock_server()
            val repository = a_repository_pointing_to(server.port)

            repository.findChartEntries(MARKET_CAP, THREE_DAYS)

            assertThat(server.takeRequest().requestUrl!!.encodedPath)
                .isEqualTo("/charts/market-cap/")
        }
    }

    @Test
    fun fetch_trade_volume_from_the_correct_path() {
        runBlocking {
            val server = a_mock_server()
            val repository = a_repository_pointing_to(server.port)

            repository.findChartEntries(TRADE_VOLUME, THREE_DAYS)

            assertThat(server.takeRequest().requestUrl!!.encodedPath)
                .isEqualTo("/charts/trade-volume/")
        }
    }

    @Test
    fun fetch_with_an_http_get() {
        runBlocking {
            val server = a_mock_server()
            val repository = a_repository_pointing_to(server.port)

            repository.findChartEntries(MARKET_PRICE, THREE_DAYS)

            assertThat(server.takeRequest().method)
                .isEqualTo("GET")
        }
    }

    @Test
    fun fetch_in_json_format() {
        runBlocking {
            val server = a_mock_server()
            val repository = a_repository_pointing_to(server.port)

            repository.findChartEntries(MARKET_PRICE, THREE_DAYS)

            assertThat(server.takeRequest().queryParams())
                .contains("format=json")
        }
    }

    @Test
    fun fetch_sampled_data() {
        runBlocking {
            val server = a_mock_server()
            val repository = a_repository_pointing_to(server.port)

            repository.findChartEntries(MARKET_PRICE, THREE_DAYS)

            assertThat(server.takeRequest().queryParams())
                .contains("sampled=true")
        }
    }

    @Test
    fun fetch_only_with_those_params() {
        runBlocking {
            val server = a_mock_server()
            val repository = a_repository_pointing_to(server.port)

            repository.findChartEntries(MARKET_PRICE, THREE_DAYS)

            assertThat(server.takeRequest().queryParams())
                .hasSize(3)
        }
    }

    @Test
    fun return_mapped_to_money_in_time() {
        runBlocking {
            val server = a_mock_server(epoch0 to price0, epoch1 to price1)
            val repository = a_repository_pointing_to(server.port)

            val marketPrice = repository.findChartEntries(MARKET_PRICE, THREE_DAYS)

            assertThat(marketPrice)
                .containsExactly(
                    MoneyToTime.from(epoch0, price0),
                    MoneyToTime.from(epoch1, price1)
                )
        }
    }

    @Test
    fun fetch_a_three_day_timespan() {
        runBlocking {
            val server = a_mock_server()
            val repository = a_repository_pointing_to(server.port)

            repository.findChartEntries(MARKET_PRICE, THREE_DAYS)

            assertThat(server.takeRequest().queryParams())
                .contains("timespan=3days")
        }
    }

    @Test(expected = RequestFailed::class)
    fun fail_when_api_connection_fails() {
        runBlocking {
            val server = a_mock_server_failing_with_400()
            val repository = a_repository_pointing_to(server.port)

            repository.findChartEntries(MARKET_PRICE, THREE_DAYS)

        }
    }

    @Test(expected = GenericInfrastructureException::class)
    fun fail_when_api_fails_withoutBody() {
        runBlocking {
            val server = a_mock_server_with_empty_body()
            val repository = a_repository_pointing_to(server.port)

            repository.findChartEntries(MARKET_PRICE, THREE_DAYS)

        }
    }

    @Test(expected = CantFindRequestBody::class)
    fun fail_when_api_fails_with_empty_json() {
        runBlocking {
            val server = a_mock_server_with_empty_json()
            val repository = a_repository_pointing_to(server.port)

            repository.findChartEntries(MARKET_PRICE, THREE_DAYS)

        }
    }

    private val epoch0 = 1613174400L
    private val price0 = 43196.48571428571
    private val epoch1 = 1613779200L
    private val price1 = 49173.612857142856
}

