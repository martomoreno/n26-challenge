package com.tinchoapps.n26.infrastructure.mother

import com.tinchoapps.n26.NetworkModule
import com.tinchoapps.n26.infrastructure.blockchain.RemoteBlockchainData
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest


internal fun RecordedRequest.queryParams() =
    requestUrl!!.encodedQuery!!.split('&')

internal fun a_repository_pointing_to(port: Int): RemoteBlockchainData {
    val retrofit = NetworkModule.providesRetrofit("http://localhost:$port", NetworkModule.providesOkHttpClient())
    return RemoteBlockchainData(retrofit)
}

internal fun a_mock_server(vararg withPrices: Pair<Long, Double> = arrayOf(1699999L to 55.86)): MockWebServer {
    val server = MockWebServer()

    server.enqueue(buildResponse(withPrices))
    server.start()

    return server
}

internal fun a_mock_server_failing_with_400(): MockWebServer {
    val server = MockWebServer()

    server.enqueue(MockResponse().setResponseCode(400))
    server.start()

    return server
}

internal fun a_mock_server_with_empty_body(): MockWebServer {
    val server = MockWebServer()
    server.enqueue(MockResponse().setResponseCode(200).setBody(""))
    server.start()

    return server
}

internal fun a_mock_server_with_empty_json(): MockWebServer {
    val server = MockWebServer()
    server.enqueue(MockResponse().setResponseCode(200).setBody("{}"))
    server.start()

    return server
}

private fun buildResponse(withPrices: Array<out Pair<Long, Double>>): MockResponse {
    val prices =
        withPrices.joinToString(",") { (epoch, price) -> """{"x": $epoch,"y": $price}""".trimIndent() }

    return MockResponse().setBody(
        """
            {
              "status": "ok",
              "name": "Market Price (USD)",
              "unit": "USD",
              "period": "day",
              "description": "Average USD market price across major bitcoin exchanges.",
              "values": [$prices]
            }
            """.trimIndent()
    )
}
